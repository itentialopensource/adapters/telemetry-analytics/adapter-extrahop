
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_21:13PM

See merge request itentialopensource/adapters/adapter-extrahop!15

---

## 0.4.3 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-extrahop!13

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:27PM

See merge request itentialopensource/adapters/adapter-extrahop!12

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_21:15PM

See merge request itentialopensource/adapters/adapter-extrahop!11

---

## 0.4.0 [07-03-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!10

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:22PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!9

---

## 0.3.2 [03-12-2024]

* Changes made at 2024.03.12_11:03AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!8

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:39AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!7

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!6

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!5

---

## 0.1.5 [03-04-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!4

---

## 0.1.4 [07-21-2020]

- Changes based on integrating to Extrahop to the healthcheck and properties (including authentication)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!3

---

## 0.1.3 [07-07-2020]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!2

---

## 0.1.2 [01-09-2020]

- Bring the adapter up to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!1

---

## 0.1.1 [12-04-2019]

- Initial Commit

See commit 5998f58

---
