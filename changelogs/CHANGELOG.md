
## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!5

---

## 0.1.5 [03-04-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!4

---

## 0.1.4 [07-21-2020]

- Changes based on integrating to Extrahop to the healthcheck and properties (including authentication)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!3

---

## 0.1.3 [07-07-2020]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!2

---

## 0.1.2 [01-09-2020]

- Bring the adapter up to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-extrahop!1

---

## 0.1.1 [12-04-2019]

- Initial Commit

See commit 5998f58

---
