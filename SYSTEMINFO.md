# ExtraHop

Vendor: ExtraHop
Homepage: https://www.extrahop.com/

Product: ExtraHop
Product Page: https://www.extrahop.com/

## Introduction
We classify ExtraHop into the Service Assurance domain as ExtraHop provides tools and endpoints for monitoring and managing network performance and configurations. 

"The platform, features, and reporting capabilities are top notch. Support and customization is fantastic."

## Why Integrate
The ExtraHop adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ExtraHop. With this adapter you have the ability to perform operations such as:

- Network
- Device Group
- Device
- Node
- Alert

## Additional Product Documentation
The [API documents for ExtraHop](https://docs.extrahop.com/9.6/rest-api-guide/)