## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for ExtraHop. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ExtraHop.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ExtraHop. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAll(callback)</td>
    <td style="padding:15px">Retrieve all activity groups from the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/activitygroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllActivityGroupDashboards(id, callback)</td>
    <td style="padding:15px">Retrieve all dashboards related to a specific activity group.</td>
    <td style="padding:15px">{base_path}/{version}/activitygroups/{pathv1}/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActivitymaps(callback)</td>
    <td style="padding:15px">Retrieve all saved activity maps.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(body, callback)</td>
    <td style="padding:15px">Create a new activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create0(body, callback)</td>
    <td style="padding:15px">Perform a network topology query.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(id, callback)</td>
    <td style="padding:15px">Delete a specific activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(id, callback)</td>
    <td style="padding:15px">Retrieve a specific activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(id, body, callback)</td>
    <td style="padding:15px">Update a specific activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create1(id, body, callback)</td>
    <td style="padding:15px">Perform a topology query for a specific activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/{pathv1}/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharing(id, callback)</td>
    <td style="padding:15px">Retrieve the users and their sharing permissions for a specific activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/{pathv1}/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharing(body, id, callback)</td>
    <td style="padding:15px">Update the users and their sharing permissions for a specific activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/{pathv1}/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceSharing(body, id, callback)</td>
    <td style="padding:15px">Replace the users and their sharing permissions for a specific activity map.</td>
    <td style="padding:15px">{base_path}/{version}/activitymaps/{pathv1}/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlerts(callback)</td>
    <td style="padding:15px">Retrieve all alerts.</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAlerts(body, callback)</td>
    <td style="padding:15px">Create a new alert with specified values.</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlertsId(id, callback)</td>
    <td style="padding:15px">Delete a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAlertsId(body, id, callback)</td>
    <td style="padding:15px">Apply updates to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedApplication(id, callback)</td>
    <td style="padding:15px">Retrieve all applications that have a specific alert assigned.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageApplicationAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific alert to applications.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignApplication(childId, id, callback)</td>
    <td style="padding:15px">Unassign an application from a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignApplication(childId, id, callback)</td>
    <td style="padding:15px">Assign an application to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDeviceGroup1(id, callback)</td>
    <td style="padding:15px">Retrieve all device groups that are assigned to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceGroupAssignments1(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific alert to device groups.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDeviceGroup1(childId, id, callback)</td>
    <td style="padding:15px">Unassign a device group from a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devicegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDeviceGroup1(childId, id, callback)</td>
    <td style="padding:15px">Assign a device group to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devicegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDevice3(id, callback)</td>
    <td style="padding:15px">Retrieve all devices that have a specific alert assigned.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceAssignments3(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific alert to devices.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDevice3(childId, id, callback)</td>
    <td style="padding:15px">Unassign a device from a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDevice3(childId, id, callback)</td>
    <td style="padding:15px">Assign a device to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedEmailGroup(id, callback)</td>
    <td style="padding:15px">Retrieve all email groups that are assigned to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/emailgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageEmailGroupAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific alert to email groups.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/emailgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignEmailGroup(childId, id, callback)</td>
    <td style="padding:15px">Unassign an email group from a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/emailgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignEmailGroup(childId, id, callback)</td>
    <td style="padding:15px">Assign an email group to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/emailgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedExclusionInterval(id, callback)</td>
    <td style="padding:15px">Retrieve all exclusion intervals assigned to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/exclusionintervals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageExclusionIntervalAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific alert to exclusion intervals.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/exclusionintervals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignExclusionInterval(childId, id, callback)</td>
    <td style="padding:15px">Unassign an exclusion interval from a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/exclusionintervals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignExclusionInterval(childId, id, callback)</td>
    <td style="padding:15px">Assign an exclusion interval to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/exclusionintervals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedNetwork(id, callback)</td>
    <td style="padding:15px">Retrieve all networks that have a specific alert assigned.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageNetworkAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific alert to networks.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignNetwork(childId, id, callback)</td>
    <td style="padding:15px">Unassign a network from a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignNetwork(childId, id, callback)</td>
    <td style="padding:15px">Assign a network to a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAdditionalStats(id, callback)</td>
    <td style="padding:15px">Retrieve all additional statistics for a specific alert.</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get0(applianceId, callback)</td>
    <td style="padding:15px">Retrieve the analysis priority rules for a specific Discover appliance.</td>
    <td style="padding:15px">{base_path}/{version}/analysispriority/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replace(body, applianceId, callback)</td>
    <td style="padding:15px">Replace the analysis priority rules for a specific Discover appliance.</td>
    <td style="padding:15px">{base_path}/{version}/analysispriority/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnalysispriorityApplianceIdManager(applianceId, callback)</td>
    <td style="padding:15px">Retrieve the appliance that manages analysis priority rules for a specific Discover appliance.</td>
    <td style="padding:15px">{base_path}/{version}/analysispriority/{pathv1}/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAnalysispriorityApplianceIdManager(body, applianceId, callback)</td>
    <td style="padding:15px">Update which appliance manages analysis priority rules for a specific Discover appliance.</td>
    <td style="padding:15px">{base_path}/{version}/analysispriority/{pathv1}/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApikeys(callback)</td>
    <td style="padding:15px">Retrieve all API keys.</td>
    <td style="padding:15px">{base_path}/{version}/apikeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApikeys(body, callback)</td>
    <td style="padding:15px">Create the initial API key for the setup user account.</td>
    <td style="padding:15px">{base_path}/{version}/apikeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApikeysKeyid(keyid, callback)</td>
    <td style="padding:15px">Retrieve information about a specific API key.</td>
    <td style="padding:15px">{base_path}/{version}/apikeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliances(callback)</td>
    <td style="padding:15px">Retrieve all remote ExtraHop appliances connected to the local appliance.</td>
    <td style="padding:15px">{base_path}/{version}/appliances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppliances(body, callback)</td>
    <td style="padding:15px">Establish a new connection to a remote ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/appliances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific remote ExtraHop appliance connected to the local appliance.</td>
    <td style="padding:15px">{base_path}/{version}/appliances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudServices(id, callback)</td>
    <td style="padding:15px">Retrieve the status of ExtraHop Cloud Services on this appliance. This method is available only on Discover appliances.</td>
    <td style="padding:15px">{base_path}/{version}/appliances/{pathv1}/cloudservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedProductKey(id, callback)</td>
    <td style="padding:15px">Retrieve the product key for a specified appliance. This request is only valid on a Command appliance.</td>
    <td style="padding:15px">{base_path}/{version}/appliances/{pathv1}/productkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplications(activeFrom, activeUntil, limit, offset, searchType = 'any', value, callback)</td>
    <td style="padding:15px">Retrieve all applications that were active within a specific timeframe.</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplications(body, callback)</td>
    <td style="padding:15px">Create a new application.</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationsId(id, includeCriteria, callback)</td>
    <td style="padding:15px">Retrieve a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApplicationsId(body, id, callback)</td>
    <td style="padding:15px">Update a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationActivity(id, callback)</td>
    <td style="padding:15px">Retrieve all activity for a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedAlert1(id, directAssignmentsOnly, callback)</td>
    <td style="padding:15px">Retrieve all alerts that are assigned to a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageAlertAssignments1(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign alerts to a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignAlert1(childId, id, callback)</td>
    <td style="padding:15px">Unassign an alert from a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert1(childId, id, callback)</td>
    <td style="padding:15px">Assign an alert to a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationDashboards(id, callback)</td>
    <td style="padding:15px">Retrieve all dashboards related to a specific application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditlog(limit, offset, callback)</td>
    <td style="padding:15px">Retrieve all audit log messages.</td>
    <td style="padding:15px">{base_path}/{version}/auditlog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAuthIDP(callback)</td>
    <td style="padding:15px">Retrieve all identity providers.</td>
    <td style="padding:15px">{base_path}/{version}/auth/identityproviders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthIDP(identityProviderParameters, callback)</td>
    <td style="padding:15px">Add an identity provider for remote authentication.</td>
    <td style="padding:15px">{base_path}/{version}/auth/identityproviders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthIDP(id, callback)</td>
    <td style="padding:15px">Delete a specific identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/auth/identityproviders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthIDP(id, callback)</td>
    <td style="padding:15px">Retrieve a specific identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/auth/identityproviders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthIDP(id, body, callback)</td>
    <td style="padding:15px">Update an existing identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/auth/identityproviders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthSAMLSPMetadata(xml, callback)</td>
    <td style="padding:15px">Retrieve SAML security provider (SP) metadata for this appliance.</td>
    <td style="padding:15px">{base_path}/{version}/auth/samlsp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBundles(callback)</td>
    <td style="padding:15px">Retrieve metadata about all bundles on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/bundles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upload(body, callback)</td>
    <td style="padding:15px">Upload a new bundle to the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/bundles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBundlesId(id, callback)</td>
    <td style="padding:15px">Delete a specific bundle.</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBundlesId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific bundle export.</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apply(id, body, callback)</td>
    <td style="padding:15px">Apply a saved bundle to the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomdevices(includeCriteria, callback)</td>
    <td style="padding:15px">Retrieve all custom devices.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomdevices(body, callback)</td>
    <td style="padding:15px">Create a custom device.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomdevicesId(id, callback)</td>
    <td style="padding:15px">Delete a specific custom device.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomdevicesId(id, includeCriteria, callback)</td>
    <td style="padding:15px">Retrieve a specific custom device.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCustomdevicesId(body, id, callback)</td>
    <td style="padding:15px">Update a specific custom device.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedCriteria(id, callback)</td>
    <td style="padding:15px">GET /customdevices/{id}/criteria has been deprecated. Replace this operation with the criteria field of the GET /customdevices and GET /customdevices/{id} operations.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices/{pathv1}/criteria?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCriteria(body, id, callback)</td>
    <td style="padding:15px">POST /customdevices/{id}/criteria has been deprecated. Replace this operation with the criteria field of the POST /customdevices and PATCH /customdevices/{id} operations.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices/{pathv1}/criteria?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCriteria(childId, id, callback)</td>
    <td style="padding:15px">DELETE /customdevices/{id}/criteria/{child-id} has been deprecated. Replace this operation with the criteria field of the PATCH /customdevices/{id} operation.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices/{pathv1}/criteria/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCriteria(childId, id, callback)</td>
    <td style="padding:15px">GET /customdevices/{id}/criteria/{child-id} has been deprecated. Replace this operation with the criteria field of the GET /customdevices and GET /customdevices/{id} operations.</td>
    <td style="padding:15px">{base_path}/{version}/customdevices/{pathv1}/criteria/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomizations(callback)</td>
    <td style="padding:15px">Retrieve all backup files.</td>
    <td style="padding:15px">{base_path}/{version}/customizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomizations(name, callback)</td>
    <td style="padding:15px">Create a backup file.</td>
    <td style="padding:15px">{base_path}/{version}/customizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomizationsStatus(callback)</td>
    <td style="padding:15px">Retrieve status details for the most recent backup attempt.</td>
    <td style="padding:15px">{base_path}/{version}/customizations/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomizationsId(id, callback)</td>
    <td style="padding:15px">Delete a specific backup file.</td>
    <td style="padding:15px">{base_path}/{version}/customizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomizationsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific backup file.</td>
    <td style="padding:15px">{base_path}/{version}/customizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomizationsIdApply(id, callback)</td>
    <td style="padding:15px">Restore only customizations from a backup file.</td>
    <td style="padding:15px">{base_path}/{version}/customizations/{pathv1}/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">download(id, callback)</td>
    <td style="padding:15px">Download a specific backup file.</td>
    <td style="padding:15px">{base_path}/{version}/customizations/{pathv1}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboards(callback)</td>
    <td style="padding:15px">Retrieve all dashboards.</td>
    <td style="padding:15px">{base_path}/{version}/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDashboardsId(id, callback)</td>
    <td style="padding:15px">Delete a specific dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDashboardsId(body, id, callback)</td>
    <td style="padding:15px">Update ownership of a specific dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardsIdSharing(id, callback)</td>
    <td style="padding:15px">Retrieve the users and their sharing permissions for a specific dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboards/{pathv1}/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDashboardsIdSharing(body, id, callback)</td>
    <td style="padding:15px">Update the users and their sharing permissions for a specific dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboards/{pathv1}/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDashboardsIdSharing(body, id, callback)</td>
    <td style="padding:15px">Replace the users and their sharing permissions for a specific dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboards/{pathv1}/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetections(limit, callback)</td>
    <td style="padding:15px">Retrieve all detections.</td>
    <td style="padding:15px">{base_path}/{version}/detections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDetectionsSearch(body, callback)</td>
    <td style="padding:15px">Search for detections.</td>
    <td style="padding:15px">{base_path}/{version}/detections/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update0(body, callback)</td>
    <td style="padding:15px">Update a ticket associated with detections.</td>
    <td style="padding:15px">{base_path}/{version}/detections/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetectionsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific detection.</td>
    <td style="padding:15px">{base_path}/{version}/detections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDetectionsId(id, body, callback)</td>
    <td style="padding:15px">Update a detection</td>
    <td style="padding:15px">{base_path}/{version}/detections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicegroups(since, all, name, callback)</td>
    <td style="padding:15px">Retrieve all device groups.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevicegroups(body, callback)</td>
    <td style="padding:15px">Create a new device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevicegroupsId(id, callback)</td>
    <td style="padding:15px">Delete a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicegroupsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDevicegroupsId(body, id, callback)</td>
    <td style="padding:15px">Update a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedAlert0(id, directAssignmentsOnly, callback)</td>
    <td style="padding:15px">Retrieve all alerts assigned to a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageAlertAssignments0(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign alerts to a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignAlert0(childId, id, callback)</td>
    <td style="padding:15px">Unassign an alert from a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert0(childId, id, callback)</td>
    <td style="padding:15px">Assign an alert to a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceGroupDashboards(id, callback)</td>
    <td style="padding:15px">Retrieve all dashboards related to a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDevice1(id, activeFrom, activeUntil, limit, offset, callback)</td>
    <td style="padding:15px">Retrieve all devices in the device group that were active within a specific time window.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceAssignments1(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign devices to a specific static device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDevice1(childId, id, callback)</td>
    <td style="padding:15px">Unassign a device to a specific static device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDevice1(childId, id, callback)</td>
    <td style="padding:15px">Assign a device to a specific static device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedTrigger(id, directAssignmentsOnly, callback)</td>
    <td style="padding:15px">Retrieve all triggers assigned to a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageTriggerAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign triggers to a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignTrigger(childId, id, callback)</td>
    <td style="padding:15px">Unassign a trigger from a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/triggers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignTrigger(childId, id, callback)</td>
    <td style="padding:15px">Assign a trigger to a specific device group.</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/triggers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(activeFrom, activeUntil, limit, offset, searchType = 'any', value, callback)</td>
    <td style="padding:15px">Retrieve all devices that were active within a specific time period.</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevicesSearch(body, callback)</td>
    <td style="padding:15px">Retrieve all devices that match specific criteria.</td>
    <td style="padding:15px">{base_path}/{version}/devices/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDevicesId(body, id, callback)</td>
    <td style="padding:15px">Update a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceActivity(id, callback)</td>
    <td style="padding:15px">Retrieve all activity for an device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedAlert2(id, directAssignmentsOnly, callback)</td>
    <td style="padding:15px">Retrieve all alerts that are assigned to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageAlertAssignments2(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific device to alerts.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignAlert2(childId, id, callback)</td>
    <td style="padding:15px">Unassign an alert from a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert2(childId, id, callback)</td>
    <td style="padding:15px">Assign an alert to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceDashboards(id, callback)</td>
    <td style="padding:15px">Retrieve all dashboards related to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDeviceGroup0(id, activeFrom, activeUntil, callback)</td>
    <td style="padding:15px">Retrieve all device groups that are assigned to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceGroupAssignments0(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific device to device groups.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDeviceGroup0(childId, id, callback)</td>
    <td style="padding:15px">Unassign a device group from a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/devicegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDeviceGroup0(childId, id, callback)</td>
    <td style="padding:15px">Assign a device group to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/devicegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedSoftware(id, from, until, callback)</td>
    <td style="padding:15px">Retrieve a list of software running on the specified device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/software?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedTag(id, callback)</td>
    <td style="padding:15px">Retrieve all tags assigned to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageTagAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific device to tags.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignTag(childId, id, callback)</td>
    <td style="padding:15px">Unassign a tag from a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignTag(childId, id, callback)</td>
    <td style="padding:15px">Assign a tag to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedTrigger0(id, directAssignmentsOnly, callback)</td>
    <td style="padding:15px">Retrieve all triggers that are assigned to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageTriggerAssignments0(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific device to triggers.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignTrigger0(childId, id, callback)</td>
    <td style="padding:15px">Unassign a trigger from a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/triggers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignTrigger0(childId, id, callback)</td>
    <td style="padding:15px">Assign a trigger to a specific device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/triggers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailgroups(callback)</td>
    <td style="padding:15px">Retrieve all email groups.</td>
    <td style="padding:15px">{base_path}/{version}/emailgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEmailgroups(body, callback)</td>
    <td style="padding:15px">Create a new email group.</td>
    <td style="padding:15px">{base_path}/{version}/emailgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEmailgroupsId(id, callback)</td>
    <td style="padding:15px">Delete a specific email group by a unique identifier.</td>
    <td style="padding:15px">{base_path}/{version}/emailgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailgroupsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific email group by a unique identifier.</td>
    <td style="padding:15px">{base_path}/{version}/emailgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEmailgroupsId(body, id, callback)</td>
    <td style="padding:15px">Apply updates to a specific email group.</td>
    <td style="padding:15px">{base_path}/{version}/emailgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExclusionintervals(callback)</td>
    <td style="padding:15px">Retrieve all exclusion intervals.</td>
    <td style="padding:15px">{base_path}/{version}/exclusionintervals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExclusionintervals(body, callback)</td>
    <td style="padding:15px">Create a new exclusion interval.</td>
    <td style="padding:15px">{base_path}/{version}/exclusionintervals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExclusionintervalsId(id, callback)</td>
    <td style="padding:15px">Delete a specific exclusion interval.</td>
    <td style="padding:15px">{base_path}/{version}/exclusionintervals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExclusionintervalsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific exclusion interval.</td>
    <td style="padding:15px">{base_path}/{version}/exclusionintervals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExclusionintervalsId(body, id, callback)</td>
    <td style="padding:15px">Apply updates to a specific exclusion interval.</td>
    <td style="padding:15px">{base_path}/{version}/exclusionintervals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadata(callback)</td>
    <td style="padding:15px">Retrieve metadata about the firmware running on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getiDrac(callback)</td>
    <td style="padding:15px">Retrieve the iDRAC IP address of the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/idrac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlatform(callback)</td>
    <td style="padding:15px">Retrieve the platform name of the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/platform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProcesses(callback)</td>
    <td style="padding:15px">Retrieve a list of processes running on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/processes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartProcess(process = 'exadmin', callback)</td>
    <td style="padding:15px">Restart a process running on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/processes/{pathv1}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regenSSLCert(callback)</td>
    <td style="padding:15px">Regenerate the SSL certificate on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/sslcert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceSSLCert(certificateAndKey, callback)</td>
    <td style="padding:15px">Replace the SSL certificate on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/sslcert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportSSLCSR(sSLCertificateSigningRequestParameters, callback)</td>
    <td style="padding:15px">Create an SSL certificate signing request</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/sslcert/signingrequest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketingStatus(callback)</td>
    <td style="padding:15px">Retrieve the ticketing integration status.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/ticketing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTicketingStatus(ticketTrackingParameters, callback)</td>
    <td style="padding:15px">Update ticket tracking settings.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/ticketing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersion(callback)</td>
    <td style="padding:15px">Retrieve the firmware version running on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/extrahop/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicense(callback)</td>
    <td style="padding:15px">Retrieve the license applied to this ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLicense(license, callback)</td>
    <td style="padding:15px">Apply and register a new license to the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProductKey(callback)</td>
    <td style="padding:15px">Retrieve the product key applied to this ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/license/productkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceProductKey(productKey, callback)</td>
    <td style="padding:15px">Apply the specified product key to the ExtraHop appliance and register the license.</td>
    <td style="padding:15px">{base_path}/{version}/license/productkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performQuery(body, callback)</td>
    <td style="padding:15px">Perform a metric query</td>
    <td style="padding:15px">{base_path}/{version}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetricsNext(xid, callback)</td>
    <td style="padding:15px">Retrieve the next chunked results from a metrics query request. This request is only valid on a Command appliance.</td>
    <td style="padding:15px">{base_path}/{version}/metrics/next/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performTotalsQuery(body, callback)</td>
    <td style="padding:15px">Perform a metric query for total values.</td>
    <td style="padding:15px">{base_path}/{version}/metrics/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performTotalByObjectQuery(body, callback)</td>
    <td style="padding:15px">Perform a metric query for total values that are grouped by object.</td>
    <td style="padding:15px">{base_path}/{version}/metrics/totalbyobject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworklocalities(callback)</td>
    <td style="padding:15px">Retrieve all network locality entries.</td>
    <td style="padding:15px">{base_path}/{version}/networklocalities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworklocalities(body, callback)</td>
    <td style="padding:15px">Create a new network locality entry</td>
    <td style="padding:15px">{base_path}/{version}/networklocalities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworklocalitiesId(id, callback)</td>
    <td style="padding:15px">Delete a specific network locality entry.</td>
    <td style="padding:15px">{base_path}/{version}/networklocalities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworklocalitiesId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific network locality entry.</td>
    <td style="padding:15px">{base_path}/{version}/networklocalities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetworklocalitiesId(body, id, callback)</td>
    <td style="padding:15px">Apply updates to a specific network locality entry.</td>
    <td style="padding:15px">{base_path}/{version}/networklocalities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(callback)</td>
    <td style="padding:15px">Retrieve all networks.</td>
    <td style="padding:15px">{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworksId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific network by ID.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetworksId(body, id, callback)</td>
    <td style="padding:15px">Update a specific network by ID.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedAlert(id, directAssignmentsOnly, callback)</td>
    <td style="padding:15px">Retrieve all alerts assigned to a specific network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageAlertAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and/or unassigned a specific network to alerts.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignAlert(childId, id, callback)</td>
    <td style="padding:15px">Unassign an alert from a specific network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert(childId, id, callback)</td>
    <td style="padding:15px">Assign an alert to a specific network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/alerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedVlan(id, callback)</td>
    <td style="padding:15px">Retrieve all vlans assigned to a specfic network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodes(callback)</td>
    <td style="padding:15px">Retrieve all Discover nodes connected to this Command appliance.</td>
    <td style="padding:15px">{base_path}/{version}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific Discover node that is connected to this Command appliance.</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNodesId(body, id, callback)</td>
    <td style="padding:15px">Update a specific Discover node that is connected to this Command appliance.</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPacketcaptures(callback)</td>
    <td style="padding:15px">Retrieve metadata about all packet captures stored on this ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/packetcaptures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePacketcapturesId(id, callback)</td>
    <td style="padding:15px">Permanently remove a specific packet capture from the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/packetcaptures/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPacketcapturesId(id, callback)</td>
    <td style="padding:15px">Download a specific packet capture in PCAP format.</td>
    <td style="padding:15px">{base_path}/{version}/packetcaptures/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPacketsSearch(output = 'pcap', limitBytes, limitSearchDuration, alwaysReturnBody, from, until, bpf, ip1, port1, ip2, port2, callback)</td>
    <td style="padding:15px">Search for packets by specifying parameters in a URL.</td>
    <td style="padding:15px">{base_path}/{version}/packets/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performPacketSearch(body, callback)</td>
    <td style="padding:15px">Search for packets by specifying parameters in a JSON string.</td>
    <td style="padding:15px">{base_path}/{version}/packets/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchCursorInBody(body, contextTtl, callback)</td>
    <td style="padding:15px">Retrieve records starting at a specified cursor.</td>
    <td style="padding:15px">{base_path}/{version}/records/cursor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchCursor(cursor, contextTtl, callback)</td>
    <td style="padding:15px">Deprecated. Replaced by POST /records/cursor.</td>
    <td style="padding:15px">{base_path}/{version}/records/cursor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performRecordQuery(body, callback)</td>
    <td style="padding:15px">Perform a record log query.</td>
    <td style="padding:15px">{base_path}/{version}/records/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRunningconfig(section, callback)</td>
    <td style="padding:15px">Retrieve the current running configuration file.</td>
    <td style="padding:15px">{base_path}/{version}/runningconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRunningconfig(body, callback)</td>
    <td style="padding:15px">Replace the current running configuration file. Configuration file changes are not automatically saved.</td>
    <td style="padding:15px">{base_path}/{version}/runningconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">save(callback)</td>
    <td style="padding:15px">Save the current changes to the running configuration file.</td>
    <td style="padding:15px">{base_path}/{version}/runningconfig/save?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRunningconfigSaved(callback)</td>
    <td style="padding:15px">Retrieve the saved running configuration file.</td>
    <td style="padding:15px">{base_path}/{version}/runningconfig/saved?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftware(softwareType, name, version, callback)</td>
    <td style="padding:15px">Retrieve all software entries.</td>
    <td style="padding:15px">{base_path}/{version}/software?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwareId(id, callback)</td>
    <td style="padding:15px">Retrieve a software entry by id.</td>
    <td style="padding:15px">{base_path}/{version}/software/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSsldecryptkeys(callback)</td>
    <td style="padding:15px">Retrieve all SSL decryption keys.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSsldecryptkeys(body, callback)</td>
    <td style="padding:15px">Create a new SSL decryption key.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSsldecryptkeysId(id, callback)</td>
    <td style="padding:15px">Remove an SSL key from the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSsldecryptkeysId(id, callback)</td>
    <td style="padding:15px">Retrieve an SSL PEM and metadata.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSsldecryptkeysId(body, id, callback)</td>
    <td style="padding:15px">Update an existing SSL decryption key.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedProtocol(id, callback)</td>
    <td style="padding:15px">Retrieve all protocols assigned to an SSL decryption key.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys/{pathv1}/protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProtocol(body, id, callback)</td>
    <td style="padding:15px">Create a new protocol for an ssl decryption key.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys/{pathv1}/protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProtocol(childId, id, port, callback)</td>
    <td style="padding:15px">Delete a protocol from an SSL decryption key.</td>
    <td style="padding:15px">{base_path}/{version}/ssldecryptkeys/{pathv1}/protocols/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupportpacks(callback)</td>
    <td style="padding:15px">Retrieve metadata about all support packs.</td>
    <td style="padding:15px">{base_path}/{version}/supportpacks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">execute(callback)</td>
    <td style="padding:15px">Run the default support pack.</td>
    <td style="padding:15px">{base_path}/{version}/supportpacks/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkQueue(id, callback)</td>
    <td style="padding:15px">Check on the status of an in-progress, running support pack.</td>
    <td style="padding:15px">{base_path}/{version}/supportpacks/queue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupportpacksFilename(filename, callback)</td>
    <td style="padding:15px">Download an existing support pack by filename.</td>
    <td style="padding:15px">{base_path}/{version}/supportpacks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTags(callback)</td>
    <td style="padding:15px">Retrieve all tags.</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTags(body, callback)</td>
    <td style="padding:15px">Create a new tag.</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagsId(id, callback)</td>
    <td style="padding:15px">Delete a specific tag.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific tag.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTagsId(body, id, callback)</td>
    <td style="padding:15px">Apply updates to a specific tag.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDevice0(id, callback)</td>
    <td style="padding:15px">Retrieve all devices assigned to a specific tag.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceAssignments0(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific tag to devices.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDevice0(childId, id, callback)</td>
    <td style="padding:15px">Unassign a device from a specific tag.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDevice0(childId, id, callback)</td>
    <td style="padding:15px">Assign a device to a specific tag.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThreatcollections(callback)</td>
    <td style="padding:15px">Retrieve metadata about all threat collections on the ExtraHop appliance.</td>
    <td style="padding:15px">{base_path}/{version}/threatcollections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postThreatcollections(userKey, name, file, callback)</td>
    <td style="padding:15px">Create a new threat collection.</td>
    <td style="padding:15px">{base_path}/{version}/threatcollections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThreatcollectionsId(id, callback)</td>
    <td style="padding:15px">Delete a threat collection from a Command appliance.</td>
    <td style="padding:15px">{base_path}/{version}/threatcollections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThreatCollectionObservables(id, callback)</td>
    <td style="padding:15px">Retrieve the number of STIX Observables loaded from a threat collection.</td>
    <td style="padding:15px">{base_path}/{version}/threatcollections/{pathv1}/observables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putThreatcollectionsUserKey(userKey, name, file, callback)</td>
    <td style="padding:15px">Upload a new threat collection, replacing any existing threat collection of the same source.</td>
    <td style="padding:15px">{base_path}/{version}/threatcollections/~{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTriggers(callback)</td>
    <td style="padding:15px">Retrieve all triggers.</td>
    <td style="padding:15px">{base_path}/{version}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTriggers(body, callback)</td>
    <td style="padding:15px">Create a new trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeExternalDataTrigger(type, body, callback)</td>
    <td style="padding:15px">Execute EXTERNAL_DATA event triggers.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/externaldata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTriggersId(id, callback)</td>
    <td style="padding:15px">Delete a specific trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTriggersId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific trigger by unique identifier.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTriggersId(body, id, callback)</td>
    <td style="padding:15px">Update an existing trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDeviceGroup(id, callback)</td>
    <td style="padding:15px">Retrieve all device groups that are assigned to a specific trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceGroupAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific trigger to device groups.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDeviceGroup(childId, id, callback)</td>
    <td style="padding:15px">Unassign a device group from a specific trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devicegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDeviceGroup(childId, id, callback)</td>
    <td style="padding:15px">Assign a device group to a specific trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devicegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDevice(id, callback)</td>
    <td style="padding:15px">Retrieve all devices that are assigned to a specific trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceAssignments(assignments, id, callback)</td>
    <td style="padding:15px">Assign and unassign a specific trigger to devices.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDevice(childId, id, callback)</td>
    <td style="padding:15px">Unassign a device from a specific trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDevice(childId, id, callback)</td>
    <td style="padding:15px">Assign a device to a specific trigger.</td>
    <td style="padding:15px">{base_path}/{version}/triggers/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroups(callback)</td>
    <td style="padding:15px">Retrieve all user groups.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroups(body, callback)</td>
    <td style="padding:15px">Create a new user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupsRefresh(callback)</td>
    <td style="padding:15px">Query LDAP for the most recent user memberships for all remote user groups.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsergroupsId(id, callback)</td>
    <td style="padding:15px">Delete a specific user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroupsId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsergroupsId(body, id, callback)</td>
    <td style="padding:15px">Update a specific user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete0(id, callback)</td>
    <td style="padding:15px">Delete all dashboard sharing associations with a specific user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedMember(id, callback)</td>
    <td style="padding:15px">Retrieve all members of a specific user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(id, assignments, callback)</td>
    <td style="padding:15px">Assign or unassign users to a user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceUser(id, assignments, callback)</td>
    <td style="padding:15px">Replace all the users assigned to the user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupsIdRefresh(id, callback)</td>
    <td style="padding:15px">Query LDAP for the most recent user membership of a specific remote user group.</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">Retrieve all users.</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsers(body, callback)</td>
    <td style="padding:15px">Create a new user.</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersUsername(username, callback)</td>
    <td style="padding:15px">Delete a specific user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsername(username, callback)</td>
    <td style="padding:15px">Retrieve a specific user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersUsername(body, username, callback)</td>
    <td style="padding:15px">Update settings for a specific user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedAPIKey(username, callback)</td>
    <td style="padding:15px">Retrieve all API keys for a specific user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPIKey(keyid, username, callback)</td>
    <td style="padding:15px">*DEPRECATED* Retrieve information about a specific API key and user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlans(callback)</td>
    <td style="padding:15px">Retrieve all VLANs.</td>
    <td style="padding:15px">{base_path}/{version}/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlansId(id, callback)</td>
    <td style="padding:15px">Retrieve a specific VLAN.</td>
    <td style="padding:15px">{base_path}/{version}/vlans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVlansId(body, id, callback)</td>
    <td style="padding:15px">Update a specific VLAN.</td>
    <td style="padding:15px">{base_path}/{version}/vlans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignDevice2(id, callback)</td>
    <td style="padding:15px">Remove a device from the whitelist (watchlist).</td>
    <td style="padding:15px">{base_path}/{version}/whitelist/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDevice2(id, callback)</td>
    <td style="padding:15px">Add a device to the whitelist (watchlist).</td>
    <td style="padding:15px">{base_path}/{version}/whitelist/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssignedDevice2(callback)</td>
    <td style="padding:15px">Retrieve all devices that are in the whitelist (watchlist).</td>
    <td style="padding:15px">{base_path}/{version}/whitelist/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageDeviceAssignments2(assignments, callback)</td>
    <td style="padding:15px">Add or remove devices from the whitelist (watchlist).</td>
    <td style="padding:15px">{base_path}/{version}/whitelist/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
